# Compatabilty with 3.6 python
import sys
import jumpcutter as jc
import threading
import os

from PySide2 import QtWidgets, QtUiTools, QtCore, QtXml, QtGui

class processVideo(object):
    def __init__(self, window):
        self.window = window
        thread = threading.Thread(target=self.run)
        thread.daemon = True
        thread.start()

    def run(self):
        if (os.path.isdir(jc.TEMP_FOLDER)): #Test to see if there is a temp file
            jc.deletePath(jc.TEMP_FOLDER)
        try:
            jc.run()
            self.window.buildProgress.setFormat("Successfully created video")
        except Exception as e:
            self.window.buildProgress.setFormat("Failed to create video - " + str(e))
        self.window.buildProgress.setMaximum(1)
        self.window.buildButton.setEnabled(True)


class Main_Window(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        loader = QtUiTools.QUiLoader()
        self.main_ui = loader.load('gui.ui', self)
        self.setCentralWidget(self.main_ui)
        self.setWindowTitle('Jumpcutter GUI')
        self.setWindowIcon(QtGui.QIcon('./cutter-minus-icon.ico'))
        self.setUnifiedTitleAndToolBarOnMac(True)
        
        self.getUIElements()
        self.makeConnections()

    def getUIElements(self):
        self.inputFileLine = self.findChild(QtWidgets.QLineEdit, "inputFileLine")
        self.inputBrowseButton = self.findChild(QtWidgets.QPushButton, "inputBrowseButton")
        self.outputBrowseButton = self.findChild(QtWidgets.QPushButton, "outputBrowseButton")
        self.outputFileLine = self.findChild(QtWidgets.QLineEdit, "outputFileLine")
        self.silentThresholdValue = self.findChild(QtWidgets.QSpinBox, "silentThresholdValue")
        self.soundedSpeedValue = self.findChild(QtWidgets.QDoubleSpinBox, "soundedSpeedValue")
        self.silentSpeedValue = self.findChild(QtWidgets.QDoubleSpinBox, "silentSpeedValue")
        self.frameMarginValue = self.findChild(QtWidgets.QSpinBox, "frameMarginValue")
        self.sampleRateValue = self.findChild(QtWidgets.QSpinBox, "sampleRateValue")
        self.sampleRateBox = self.findChild(QtWidgets.QCheckBox, "sampleRateCheckbox")
        self.frameRateValue = self.findChild(QtWidgets.QSpinBox, "frameRateValue")
        self.frameRateBox = self.findChild(QtWidgets.QCheckBox, "frameRateCheckBox")
        self.frameQuality = self.findChild(QtWidgets.QSlider, "frameQualityValue")
        self.buildProgress = self.findChild(QtWidgets.QProgressBar, "BuildProgress")
        self.buildButton = self.findChild(QtWidgets.QCommandLinkButton, "RunButton")

        for item in vars(self).items(): #Verify all GUI elements are found before continuing in code.
            if item[1] == None:
                raise Exception("GUI element ["+item[0]+"] could not be found.")

    def makeConnections(self):
        self.inputBrowseButton.clicked.connect(self.browseOpenFiles)
        self.outputBrowseButton.clicked.connect(self.browseSaveFiles)
        self.buildButton.clicked.connect(self.buildFile)

    def browseOpenFiles(self):
        if (self.outputFileLine.text()):
            fileType = '*' + self.outputFileLine.text()[-4:]
        else:
            fileType = ''
        file = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', '', fileType)
        if file[0] != '':
            self.inputFileLine.setText(file[0])

    def browseSaveFiles(self):
        if (self.inputFileLine.text()):
            fileType = '*' + self.inputFileLine.text()[-4:]
        else:
            fileType = ''
        file = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File', '', fileType)
        if file[0] != '':
            self.outputFileLine.setText(file[0])

    def buildFile(self):
        #Get all the values need to run
        self.buildButton.setEnabled(False)
        self.buildProgress.setFormat("Locking in Variables")
        self.buildProgress.setMaximum(0)
        if self.inputFileLine.text() == '':
            self.buildProgress.setFormat("Failed to create video - need input video")
            self.buildProgress.setMaximum(1)
            return
        jc.setInputFile('"'+self.inputFileLine.text()+'"')
        if self.outputFileLine.text() == '':
            self.buildProgress.setFormat("Failed to create video - need output video")
            self.buildProgress.setMaximum(1)
            return
        jc.setOutputFile('"'+self.outputFileLine.text()+'"')
        jc.setSilentThreshold((self.silentThresholdValue.value() / 100))
        jc.setSpeed(self.silentSpeedValue.value(),self.soundedSpeedValue.value())
        jc.setFrameMargin(self.frameMarginValue.value())
        if self.sampleRateBox.isChecked():
            jc.setSampleRate(self.sampleRateValue.value())
        if self.frameRateBox.isChecked():
            jc.setFrameRate(self.frameRateValue.value())
        jc.setFrameQuality(self.frameQuality.value())
        self.buildProgress.setFormat("Building Video, Please Wait")
        processingVideo = processVideo(self)
        

    
    def quit(self):
        print("Ended Session")

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    win = Main_Window()
    win.show() #How do I use win??????
    app.connect(app, QtCore.SIGNAL('aboutToQuit()'),win.quit)
    app.exec_()
